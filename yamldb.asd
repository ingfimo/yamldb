(asdf:defsystem "yamldb"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on (:cl-yaml
	       :cl-ppcre
	       :alexandria)
  :components ((:module "src"
                :components
			((:file "commands")
			 (:file "statements")
			 (:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "yamldb/tests"))))

(asdf:defsystem "yamldb/tests"
  :author ""
  :license ""
  :depends-on ("yamldb"
               "rove")
  :components ((:module "tests"
                :components
		((:file "commands")
		 (:file "statements")
		 (:file "main"))))
  :description "Test system for yamldb"
  :perform (asdf:test-op (op c) (symbol-call :rove :run c)))
