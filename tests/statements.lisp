(defpackage yamldb/tests/statements
  (:use
   :cl
   :rove)
  (:import-from
   :alexandria
   :hash-table-alist)
  (:import-from
   :yamldb/statements
   :from
   :extract
   :select
   :del
   :insert
   ))
(in-package :yamldb/tests/statements)

(defvar *yaml1* (asdf:system-relative-pathname :yamldb/tests "tests/data/yaml1.yaml"))

(deftest test-from-extract
  (testing "if from commands read correctly the following path should exist"
    (let ((y (funcall (from *yaml1*) :root NIL)))
      (ok (equal (funcall (extract ":root/a") (first y) (second y))  '(:extract 1)))
      (ok (equal (funcall (extract ":root/d/$0/a") (first y) (second y)) '(:extract "tizio")))
      (ok (equal (funcall (extract ":root/f/i/a") (first y) (second y)) '(:extract "franco")))
      )
    )
  )

(deftest test-from-del
  (testing ""
    (let ((y (funcall (from *yaml1*) :root NIL)))
      (let ((y1 (funcall (del ":root/d/$0/b") (first y) (second y))))
	(ok (equal (funcall (extract ":root/d/$0/a") (first y1) (second y1))
		   '(:extract "tizio")))
	)
      (let ((y1 (funcall (del ":root/e/$0") (first y) (second y))))
	(ok (equal (funcall (extract ":root/e") (first y1) (second y1))
		   `(:extract (("u" "v" "z")))))
	)
      )
    )
  )

(deftest test-from-insert
  (testing ""
    (let ((y (funcall (from *yaml1*) :root NIL)))
      (let ((y1 (funcall (insert "pizza" ":root/h") (first y) (second y))))
	(ok (equal (funcall (extract ":root/h") (first y1) (second y1)) '(:extract "pizza")))
	)
      (let ((y1 (funcall (insert "rossi" ":root/d/$1/c") (first y) (second y))))
	(ok (equal (funcall (extract ":root/d/$1/c") (first y1) (second y1)) '(:extract "rossi")))
	)
      (let ((y1 (funcall (insert "q" ":root/e/$0/$0") (first y) (second y))))
	(ok (equal (funcall (extract ":root/e/$0") (first y1) (second y1))
		   `(:extract ("q" "r" "s" "t"))))
	)
      )
    )
  )

