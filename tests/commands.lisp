(defpackage yamldb/tests/commands
  (:use
   :cl
   :rove)
  (:import-from
   :alexandria
   :hash-table-alist)
  (:import-from
   :yamldb/commands
   :*from
   :*write-yaml
   :*del
   :*insert
   :*select
   :*extract
   ))
(in-package :yamldb/tests/commands)

(defvar yaml1 (asdf:system-relative-pathname :yamldb/tests "tests/data/yaml1.yaml"))

(deftest test-*from-*extract
  (testing "if *from commands read correctly the following path should exist"
	   (let ((y (second (*from (list yaml1)))))
	     (ok (equal (*extract (list y ":root/a")) '(:extract 1)))
	     (ok (equal (*extract (list y ":root/d/$0/a")) '(:extract "tizio")))
	     (ok (equal (*extract (list y ":root/f/i/a")) '(:extract "franco")))
	     )
	   )
  )

(deftest test-*from-*del
  (testing ""
    (let ((y (second (*from (list yaml1)))))
      (let ((y1 (second (*del (list y ":root/d/$0/b")))))
	(ok (equal (*extract (list y1 ":root/d/$0/a")) '(:extract "tizio")))
	)
      (let ((y1 (second (*del (list y ":root/e/$0")))))
	(ok (equal (*extract (list y1 ":root/e")) `(:extract (("u" "v" "z")))))
	)
      )
    )
  )

(deftest test-*from-*insert
  (testing ""
    (let ((y (second (*from (list yaml1)))))
      (let ((y1 (second (*insert (list y "pizza" ":root/h")))))
	(ok (equal (*extract (list y1 ":root/h")) '(:extract "pizza")))
	)
      (let ((y1 (second (*insert (list y "rossi" ":root/d/$1/c")))))
	(ok (equal (*extract (list y1 ":root/d/$1/c")) '(:extract "rossi")))
	)
      (let ((y1 (second (*insert (list y "q" ":root/e/$0/$0")))))
	(ok (equal (*extract (list y1 ":root/e/$0")) `(:extract ("q" "r" "s" "t"))))
	)
      )
    )
  )
   
	
	
