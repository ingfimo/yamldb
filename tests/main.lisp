(defpackage yamldb/tests/main
  (:use
   :cl
   :rove
   :cl-yaml)
  (:import-from
   :yamldb
   :yamldb>)
  (:import-from
   :yamldb/statements
   :from
   :insert
   :del
   :show
   :select
   :extract)
  )
(in-package :yamldb/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :yamldb)' in your Lisp.

(defvar *yaml1* (asdf:system-relative-pathname :yamldb/tests "tests/data/yaml1.yaml"))
(defvar *yaml2* (asdf:system-relative-pathname :yamldb/tests "tests/data/yaml2.yaml"))


(deftest test-session
  (testing "should (= 1 1) to be true"
    (ok (equal
	 (yaml:emit-to-string
	  (yamldb>
	   (from *yaml1*)
	   (insert "ignazio" ":root/d/$0/c")
	   (insert "filiberto" ":root/d/$0/d")
	   (del ":root/d/$1/b")
	   (del ":root/e/$0/$1")
	   (insert "w" ":root/e/$1/$2")
	   (insert "x" ":root/e/$1/$4")
	   (insert 7 ":root/f/l")
	   (insert "fragole" ":root/g/$0")
	   (insert "arrivederci" ":root/h")
	   (extract ":root"))
	  )
	 (yaml:emit-to-string
	  (yamldb>
	   (from *yaml2*)
	   (extract ":root"))
	  )
	 ))
    )
  )
