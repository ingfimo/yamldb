(defpackage yamldb/statements
  (:nicknames :ydbs)
  (:use
   :cl)
  (:import-from
   :yamldb/commands
   :*from
   :*write-yaml
   :*show
   :*del
   :*insert
   :*select
   :*extract
   )
  (:export
   :from
   :write-yaml
   :show
   :del
   :insert
   :select
   :extract))
(in-package :yamldb/statements)


(defmacro defstatement (statement cmd &rest required)
  `(defun ,statement (&rest args)
      (lambda (prev y)
	(if (equal prev :root) (funcall ,cmd args)
	    (if (equal (car ,required) :any) (funcall ,cmd (cons y args))
		(if (find prev '(,@required)) (funcall ,cmd (cons y args))
		    (error "Statement requirements not satisfied")
		    )
		)
	    )
	)
     )
  )

(defstatement from #'*from :root)

(defstatement write-yaml #'*write-yaml :any)

(defstatement show #'*show :any)

(defstatement del #'*del :any)

(defstatement insert #'*insert :any)

(defstatement select #'*select :any)

(defstatement extract #'*extract :any)
