(defpackage yamldb
  (:nicknames :ydb)
  (:use
   :cl
   :cl-yaml
   :cl-ppcre
   :alexandria)
  (:import-from
   :yamldb/statements
   :from
   :write-yaml
   :del
   :insert
   :select)
  (:export
   :yamldb>
   :from
   :write-yaml
   :show
   :del
   :insert
   :select
   :extract))
(in-package :yamldb)

(defun yamldb-recur (prev y sts)
  (if (car sts)
      (let ((out (funcall (car sts) prev y))
	    (follows (cdr sts)))
	(yamldb-recur (first out) (second out) follows)
	)
      y
      )
  )

(defun yamldb> (&rest sts)
  (yamldb-recur :root NIL sts)
  )
