(defpackage yamldb/commands
  (:nicknames :ydbc)
  (:use
   :cl
   :cl-yaml
   :cl-ppcre
   :alexandria)
  (:export
   :*from
   :*write-yaml
   :*show
   :*del
   :*insert
   :*select
   :*extract))
(in-package :yamldb/commands)

(defun wrap-with-root (ht)
  (let ((wrapper (make-hash-table)))
    (setf (gethash :root wrapper) ht)
    wrapper
    )
  )

(defun parse-yaml-path (path-string)
  (let ((path-list (cl-ppcre:split "/" path-string)))
    (mapcar (lambda (x) (cond 
			  ((cl-ppcre:scan "\\$" x) (parse-integer (subseq x 1)))
			  ((cl-ppcre:scan ":" x) (read-from-string x))
			  (T x)
			  ))
	    path-list)
    )
  )

;; From

(defun *from (args)
  "Parses a YAML/JSON string or file."
  (let ((yaml-string-or-path (first args)))
    (list :from (wrap-with-root (yaml:parse yaml-string-or-path)))
    )
  )

;; Write-YAML

(defun *write-yaml (args)
  "Writes an hash-table to a file in YAML format"
  (let ((y (first args)) (path (second args)))
    (let ((yy (gethash :root y)))
      (with-open-file (outf (pathname path))
	:direction :output
	:if-exists :supersede
	(yaml:with-emitter-to-stream (em outf) (yaml:emit-pretty-as-document em yy))
	)
      (list :write-yaml y)
      )
    )
  )

;; Show-YAML

(defun *show (args)
  (let ((y (first args)))
    (let ((yy (gethash :root y)))
      (print (yaml:with-emitter-to-string (em) (yaml:emit-pretty-as-document em yy)))
      )
    (list :show y)
    )
  )

;; Set

(defun sety-ith (seq ith x)
  (setf (nth ith seq) x)
  seq
  )

(defun sety-key (ht k x)
  (setf (gethash k ht) x)
  ht
  )

(defgeneric sety (input ith-or-k x)
  (:documentation "Some doc")
  )

(defmethod sety ((input cons) ith-or-k x)
  (sety-ith input ith-or-k x)
  )

(defmethod sety ((input hash-table) ith-or-k x)
  (sety-key input ith-or-k x)
  )

;; Get

(defgeneric gety (input ith-or-k)
  (:documentation "Some doc")
  )

(defmethod gety ((input cons) ith-or-k)
  (nth ith-or-k input)
  )

(defmethod gety ((input hash-table) ith-or-k)
  (gethash ith-or-k input)
  )

;; Del

(defun del-ith (seq ith)
  "Removes the ith element from a sequence"
  (remove-if (lambda (x) x) seq :start ith :count 1))

(defgeneric del-element (input ith-or-k)
  (:documentation "Some doc")
  )

(defmethod del-element ((input cons) ith-or-k)
  (del-ith input ith-or-k)
  )

(defmethod del-element((input hash-table) ith-or-k)
  (remhash ith-or-k input)
  input
  )

(defun del-recur (y path)
  (if (eql (length path) 1)
      (del-element y (car path))
      (sety y (car path) (del-recur (gety y (car path)) (cdr path)))
      )
  )

(defun *del (args)
  (let ((y (first args)) (path-string (second args)))
    (let ((path (parse-yaml-path path-string)))
      (list :del (del-recur y path))
      )
    )
  )

;; Insert

(defun insert-at (seq x at)
  "Inserts an element into a sequence at a certain position"
  (if (eql 0 at) (cons x seq)
      (if (eql at (length seq)) (append seq (list x))
         (let ((a (subseq seq 0 at)) (b (subseq seq at)))
           (append a (cons x b))
	   )
	 )
      )
  )

(defun insert-key (ht x k)
  "Inserts an element in an hash table at the given key"
  (setf (gethash k ht) x)
  ht
  )

(defgeneric insert-element (input x at-or-k)
  (:documentation "Some doc")
  )

(defmethod insert-element ((input cons) x at-or-k)
    (insert-at input x at-or-k)
  )

(defmethod insert-element ((input hash-table) x k)
  (insert-key input x k))

(defun insert-recur (y x path)
  (if (eql (length path) 1)
      (insert-element y x (car path))
      (sety y (car path) (insert-recur (gety y (car path)) x (cdr path)))
      )
  )

(defun *insert (args)
  (let ((y (first args)) (x (second args)) (as (third args)))
    (let ((path (parse-yaml-path as)))
      (list :insert (insert-recur y x path))
      )
    )
  )

;; Select / Extract

(defun extract-recur (y path)
  (if (eql (length path) 0)
      y
      (extract-recur (gety y (car path)) (cdr path))
      )
  )

(defun *select (args)
  (let ((y (first args)) (path-string (second args)))
    (let ((path (parse-yaml-path path-string)))
      (list :select (wrap-with-root (extract-recur y path)))
      )
    )
  )

(defun *extract (args)
  (let ((y (first args)) (path-string (second args)))
    (let ((path (parse-yaml-path path-string)))
      (list :extract (extract-recur y path))
      )
    )
  )
